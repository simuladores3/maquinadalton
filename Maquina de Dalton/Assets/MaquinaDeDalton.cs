using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaquinaDeDalton : MonoBehaviour
{
    public GameObject objetoPrefab; // Arrastra tu prefab de objeto aquí

    public int filas = 5; // Número de filas de objetos
    public float espacioEntreObjetos = 1.0f; // Espacio entre los objetos

    void Start()
    {
        GenerarPiramide();
    }

    void GenerarPiramide()
    {
        if (objetoPrefab == null)
        {
            Debug.LogError("Asigna el prefab del objeto en el Inspector.");
            return;
        }

        Vector3 startPosition = transform.position;
        Vector3 spawnPosition = startPosition;

        for (int fila = 0; fila < filas; fila++)
        {
            for (int i = 0; i <= fila; i++)
            {
                GameObject objeto = Instantiate(objetoPrefab, spawnPosition, new Quaternion(0,0,-45,0));
                objeto.transform.Rotate(new Vector3(0, 0, -45));
                objeto.transform.SetParent(transform); 

                spawnPosition.x += espacioEntreObjetos * Mathf.Sqrt(3); // Desplazamiento en x para formar un triángulo equilátero
            }

            spawnPosition.x = startPosition.x - (espacioEntreObjetos / 2) * (fila + 1) * Mathf.Sqrt(3); // Ajuste para la siguiente fila
            spawnPosition.y += espacioEntreObjetos * 1.5f; // Desplazamiento en y para formar un triángulo equilátero
        }

        this.gameObject.transform.rotation = new Quaternion(0, 0, 0,0);
    }
}