using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioColor : MonoBehaviour
{
    [Header ("Objetos a Pintar")]

    [SerializeField] GameObject ParedFondo;
    [SerializeField] List<MeshRenderer> Obstaculos;
    [SerializeField] List<MeshRenderer> Paredes;
    [SerializeField] List<MeshRenderer> Pinchos;

    [SerializeField] public List<ModeColor> Modo;


    void Start()
    {
    }

    void Update()
    {
        
    }

    public void CambiarColor()
    {
        ParedFondo.GetComponent<MeshRenderer>().material = Modo[0].FondoMaquina;

        Camera c = FindObjectOfType<Camera>();
        c.backgroundColor = Modo[0].Fondo;

        SpawnerBalls.colorBall = Modo[0].ColorPelotas;

        foreach (MeshRenderer mesh in Obstaculos)
        {
            mesh.material = Modo[0].ColorObstaculos;
        }

        foreach (MeshRenderer mesh in Paredes)
        {
            mesh.material = Modo[0].Paredes;
        }

        foreach (MeshRenderer mesh in Pinchos)
        {
            mesh.material = Modo[0].Pinchos;
        }

    }

   
}
