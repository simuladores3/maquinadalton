using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBalls : MonoBehaviour
{
    [SerializeField] private float timePerSpawn;
    private float time;



    private bool Act;

    [SerializeField] private GameObject ballPreFab;

    [Header("Spawns")]

    [SerializeField] private Transform SpawnDerecha;
    [SerializeField] private Transform SpawnIzquierda;

    private int spawn;
    [SerializeField] private int CantidadTandasSpawn;

    public static Material colorBall;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Act = true;
        }

        
        SpawnearPelotas();
    }

    public void SpawnearPelotas()
    {
        if (Act)
        {
            if (time < timePerSpawn)
            {
                time += Time.deltaTime;
            }
            else
            {
                GameObject d =  Instantiate(ballPreFab, SpawnDerecha.position , ballPreFab.transform.rotation);
                GameObject i = Instantiate(ballPreFab, SpawnIzquierda.position, ballPreFab.transform.rotation);

                d.GetComponent<MeshRenderer>().material = colorBall;
                i.GetComponent<MeshRenderer>().material = colorBall;

                spawn++;
                time = 0;
            }

            if(spawn >= CantidadTandasSpawn)
            {
                Act = false;
            }
        }
    }
}
